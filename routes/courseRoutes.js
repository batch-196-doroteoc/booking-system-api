	// routes
	// the Router() method, will allow us to contain our routes
const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;


console.log(courseControllers);
//import the course model so we can manipualte ut and add a new course doc
// const Course =require("../models/Course");
//All routes to courses not has an endpoint prefaced with /courses
//endpoint - /courses/
// router.get('/courses',(req,res)=>{
// 	res.send("This route will get all course documents");
// });
// router.post('/courses',(req,res)=>{
// 	res.send("This route will create a new course document");
// })
//import the course model so we can manipulate it and add a new course document
//gets all course documents whether it is active or inactive
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);
//only logged in user is able to use addCourse
//
router.post('/',verify,verifyAdmin,courseControllers.addCourse);

//get all active courses - (regular,non-logged in)
router.get('/activeCourses',courseControllers.getActiveCourses);


////activity
///we can pass data in a royte without the use of request body by passing a small amount of data through the url with the use of route params like :id
// /endpoint/:routeParams
///http://localhost:4000/courses/getSingleCourse/62e7632a2ee77dceb8316235
router.get('/getSingleCourse/:courseId',courseControllers.getSingleCourse);

//update a single course
//pass the id of the course we want to update via route params
//the update details will be passed via request body
router.put('/updateCourse/:courseId',verify,verifyAdmin,courseControllers.updateCourse);
//archive a single course
//pass the id for the course we want to update via route params
//we will directly update the course as inactive

router.delete('/archiveCourse/:courseId',verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;