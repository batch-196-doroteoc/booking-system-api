//import model first
const User = require("../models/User");
//import course model
const Course = require("../models/Course");
//hash passwords
const bcrypt = require("bcrypt");
//import auth.js module to use createAccessToken and its subsequent methods
const auth = require("../auth");
//paste methods here
module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};

//get user Details should only allow the logged in user to get his own details
module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
	// console.log(req.body)
	// User.find({"_id":req.body.id},{"_id":0,"password":0,"isAdmin":0,"enrollments":0})
	// User.findOne({_id:req.body.id})
	// User.findById(req.body.id)
	User.findById(req.user.id)
	//is a mongoose method that will allow us to find a document strictly by its id
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};


module.exports.loginUser = (req,res)=>{
	console.log(req.body);

	/*
		Steps for logging in our user:
		1.find the user by its email
		2. if we found the user, we will check his password if the password input and the hashed password in our db matches
		3. if we don't find a user, we will send a message to the client.
		4. if upon checking the found user's password is the same our input password, we will generate a "key" for the user to have authorization to access certain features in our app

	*/
	//mongodb: db.users.findOne({email:})
	User.findOne({email:req.body.email})
	.then(foundUser=>{
		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if it is not able to match any document
		if(foundUser === null){
			return res.send({message: "No User Found."})//client will receive this object with our message if no user is found
		}else{
			// console.log(foundUser)
			//if we find a user, foundUser will contain the dcouemnt that macthed the input email
			//check if the input password from req.body meatches the hashed password in our foundUser document
			/*bcrypt.compareSync(<inputString>,<hashedString>)
			"spiderman06"
			"passorfdssf"

			If the inputString and the hashedString matches and are the same, the compareSync method will return true. else, it will return false
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			// console.log(isPasswordCorrect);
			//if the password is correctm we will create a "key", a token for our user, else, we will send a message
			if(isPasswordCorrect){

				/*
					To be able to create a key or token that authorizes our logged in user around our app, we have to create our own module called auth.js
					this moduele will create an encoded string which contains our user's details
					this encoded string is what we call a JSONWebToken or JWT,
					this JWT can only be properly unwrapped or decoded with our own secret word/string
				*/

				// console.log("We will create a token for the user if the password is correct")
				//auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details is JWT...
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			}else{
				return res.send({message:"Incorrect Password"});
			}


		}


	})

};

//checks if email exists or not
module.exports.checkWithEmail = (req,res) =>{
	// console.log(req.body)
	User.findOne({email:req.body.email})
	.then(result=>{
		//findOne will return null if no match is found
		//send false if wala, email doesnt exist
		//sends true if meron, email exists
		if(result===null){
			return res.send(false)
		}else{
			return res.send(true)
		}
	})
	.catch(error=>res.send(error))
};

module.exports.enroll = async (req,res) => {
	//check the id of the user?
	///console.log(req.user.id);
	//check the id of the course we want to enroll?
	///console.log(req.body.courseId);
	//validate the user if they are an admin or not
	//if the user is an admin, send a message to client and end the reponse
	//else, we will continue
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden!"});
	}


	/*
	enrollment will come in 2 steps
	first, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.
	second, find the course where we are enrolling and update its ernolless subdocument array, we will push the userId in the enrolless array,

	since we will acess 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line
	async and await - async keyword is added to a function to make our function asynchronous. which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result of a function
	
	to be able to wait for the result of a function, we use the await keyword. the await keyword allows us to wait fo the function to finish and get a result before proceeding

	*/

	//return a boolean to our isUserUpdatedVariable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array
	let isUserUpdated = await User.findById(req.user.id).then(user=>{
		console.log(user);
	
	//check if you found the user's document:
	//console.log(user);
	//add the courseId an object and push that object into the user's enrollments
	//because we have to follow the schema of the enrollments subdocument array"
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//access the enrollments array from our user and push the new enrollment subdocument into the enrollments array

		user.enrollments.push(newEnrollment);
		//we must save the user document
		return user.save().then(user=>true).catch(err=>err.message)
	})
	//if user was able to enroll properly,isUserUpdated contains true
	// console.log(isUserUpdated);
	//Add an if statement and stop the process IF isUserUpdated does not contain true
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	//find the course where we will enroll or add the user as an enrollee and return true IF we were able to push the user into the enrollees array properly or send the error message instead
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course=>{

		//console.log(course); //contain the found course we want to enroll in

		//create an object to be pushed into the subdocument array, enrollees.
		//we have to follow the schema of our subdocument array
	let enrollee = {
		userId: req.user.id
	}
		//push the enrollee into the enrollees subdocument array of the course:
		course.enrollees.push(enrollee);
		//save the course document
		//return true IF we were able to save and add the user as enrollee properly
		//return an err message if we catch an error
		return course.save().then(course=>true).catch(err=>err.message);
	})
	// console.log(isCourseUpdated);
	//if the isCourseUpdated does not contain true, send the err message to the client and stop the process
	if(isCourseUpdated !== true){
		return res.send({message:isCourseUpdated})
	}

	//ensure that both update the user and course document to add our enrollment and enrolee respectively and send a message to the client to end the enrollemnt process:

	if(isUserUpdated && isCourseUpdated){
		return res.send({message:"Thank you for enrolling!"})
	}


}