//import the Course model so we can manipulate it and add a new course docuemnt
const Course =require("../models/Course");

module.exports.getAllCourses = (req,res)=>{


	//use the course model to connect to out collection and retrieve our courses
	//to be able to query into out collections we use the model connected to that collection
	//inmongoDB: db.courses.find({})
	//ito na siya
	Course.find({})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

	// res.send("This route will get all course documents");
};

module.exports.addCourse = (req,res)=>{
	// res.send("This route will create a new course document");
	// console.log(req.body);
	//using the Course model, we will use its constructor to create our Course docuemnt which will follow the schema of the model, and add methods for document creation.
	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})
	// console.log(newCourse);
		//newcourse is now an object which follows the courseSchema and with additional methods from our Course constructor
		//.save() method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
		//db.courses.insertOne()
		//pano tayo nakaconnect sa collection gawa ng model
		//.then() allows us to process the result of a previous function/method in its own anonymous function
	newCourse.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

module.exports.getActiveCourses = (req,res) =>{

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};
///////Activity
module.exports.getSingleCourse = (req,res)=>{
	console.log(req.params)
	//req.params is an object that contains the value captured via route params
	//the field name of the req.params indicate the name of the route params
	//How do we get the id passed as the route params?
	console.log(req.params.courseId)
	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.updateCourse = (req,res) =>{
	//how do we check if we can get the id
	console.log(req.params.courseId);
	//how do we check the update that is input?
	console.log(req.body);

	//findByIdAndUpdate - used to update documents and has 3 arguments
	//findByIdAndUpdate(<id>,{updates},{new:true})

	//we can create a new object to filter update details
	//the indicated fields in the update object will be the fields updated
	//fields/property that are not part of the update object will not be updated

	let update = {
		name:req.body.name,
		description:req.body.description,
		price:req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result=> res.send(result))
	.catch(error=> res.send(error))

};

module.exports.archiveCourse = (req,res) => {

	console.log(req.params.courseId);
	// console.log(req.body);
	//update the course document to inactive for "soft delete"
	let update = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

// module.exports.archiveCourse = (req,res) => {

// 	console.log(req.params.courseId);
// 	// console.log(req.body);
// 	//update the course document to inactive for "soft delete"

// 	Course.findByIdAndUpdate(req.params.courseId,{isActive:true},{new:true})
// 	.then(result=>res.send(result))
// 	.catch(error=> res.send(error))
// };