const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
//bcrypt adds security for user details
const userControllers = require("../controllers/userControllers");
// console.log(userControllers);

//import auth to be able to have access and use the verify methods to act as middleware for our routes
//Middleware add in the toute such as verify() will have access to the req,res objects
const auth = require("../auth");
//destructure auth to get our methods and save it in variables//kinuha lang natin ang function sa loob ng auth file
const {verify} = auth;

//register
router.post("/",userControllers.registerUser);
//verify() is used as a middleware which means our request will eget throigh verify first before our controller//gate//bouncer//no token no entry
//verify() will not only check the validity of the token but also add the decoded data of the token in the request obj as req.user

router.get('/details',verify,userControllers.getUserDetails);
//dadaan muna siya sa verify

//route for user authentication
router.post('/login',userControllers.loginUser);

///////////////Activitycheckemail
router.post('/checkEmail',userControllers.checkWithEmail);

///user enrollment
//courseId will come from the req.body
//userId will come from req.user
router.post('/enroll',verify,userControllers.enroll);

module.exports = router;

