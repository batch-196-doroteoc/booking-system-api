const express = require("express");
// Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database
const mongoose = require("mongoose");
const app = express();
const port = 4000;

/*
	Mongoose Connection
	
	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. First, is the connection string to connect our api to our mongodb. Second is an object used to add info between mongoose and mongodb
		
*/
mongoose.connect("mongodb+srv://admin:admin123@cluster0.gnqqlit.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;
//This is to show notification of an internal server error from MongoDB
db.on('error',console.error.bind(console,"MongoDB Connection Error."));
//if the connection is open and successful, we will ouput a message in the terminal/gitbash
db.on('open',()=>console.log("Connected to MongoDB."))

//express.json() to be able to handle the request body and parse it into JS Object
app.use(express.json());
//import our routes and use it as middleware
//which means that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);

// console.log(courseRoutes); - delete
//to use our routes and group them together under '/courses'----our endpoints are now prefaced with /courses
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);



// app.get('/courses',(req,res)=>{
// 	res.send("This route will get all course documents");
// });
// app.post('/courses',(req,res)=>{
// 	res.send("This route will create a new course document");
// })
// app.delete('/courses',(req,res)=>{
// 	res.send("This route will delete courses");
// }) ---deleted na kasi nilagay na siya sa courseRoutes
app.listen(port,()=>console.log(`Server is running at port ${port}`));