/*auth.js - module which will contain methods to help authorize or restrict users from accessing certain features in our application */

const jwt = require("jsonwebtoken");
//this is the secret string which will validate or which will use to check the validity of a passed token. if a token does not contain this secret string, then that token is invalid or illegitimate
const secret = "courseBookingAPI";

module.exports.createAccessToken = (userDetails) =>{

	//pick only certain details from our user to be included in the token,
	//password should not be included!!!!!!
	// console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin:userDetails.isAdmin
	}

	console.log(data);
	//jwt.sign() will create a JWT using our data object, with our secret
	return jwt.sign(data,secret,{});
}


module.exports.verify = (req,res,next) => {

	//verify() is going to be used as a middleware, wherein it will be added per route to act as a gate to check if the token being passed is valid or not.
	//this will also allow us to check if the user is allowed to access the feature or not
	//we will also check the validity of the token using its secret
	//we will pass the token with our request headers as authorization
	//requests that need a token must be able to pass the token in the authorization headers
	let token = req.headers.authorization
	//if the token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."})
	}else{

		/*
			When passing JWT, we use the Bearer Token authorization. This means that wehn JWY is passed a word, 'bearer' as well as a space is added

			Bearer hiuhiuhhghckclgvliyf
			0123456--7
			slice() and copy the rest of the token without the word bearer

			slice(<startingPosition>,<endPosition>)
			Update the token varaible with the sliced version

		*/
		// console.log(token);
		token = token.slice(7);
		// console.log(token);
		//verify the validity of a token by cheking the overall length of the token and if the token contains the secret
		//it has 3 arguments the token, secret, and a handler function which will handle either an error if the token is invalid or the decoded dat from the token
		jwt.verify(token,secret,function(err,decodedToken){
			// console.log(decodedToken);//contains the data of the token if the token is verified as legitimate
			// console.log(err);
			//contains null if the token is legit

			//send a message to our client if there is an error or add our decodedToken to our requestObject which we can then pass to the next controller/middleware

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			}else {
				//add a new user property in the request object and add the decoded token as its value
				//Therefore, the next controller or middleware will not have access to the id, email, and isAdmin properties of the logged in user
				req.user = decodedToken;
				//tayo naglagay ng user property para malagay ang details ng token
				//next() this will let us proceed tot he next middleware or controller
				next();

			}
		})
	}
}

	//verifyAdmin will be used as a middleware.
	//it has to follow or be added after verify(), so that we can check for the validity and add the decodedToken is the request object as req.user
	module.exports.verifyAdmin = (req,res,next)=>{
		//verifyAdmin must come afterverify ot have access to req.user
		console.log(req.user);

		if(req.user.isAdmin){
			//if the user is admin, procced to the next middleware or controller,
			next();
		}else{

			return res.send({
				auth:"Failed",
				message:"Action Forbidden!"
			})
		}
}